package harjutamine;

public class Massiiv {

	public static void main(String[] args) {
		
//		Meetod (alamprogramm)
//
//		Java rakendus sisaldab p�hiprogrammi (main), millest t�en�oliselt p��rdutakse ka mingite alamprogrammide poole. Javas nimetatakse alamprogramme meetoditeks (tulenevalt selle keele objektorienteeritusest) ning meetodid on r�hmitatud klasside kaupa. Meetodid v�ivad olla kas programmeerija enda poolt loodud v�i Javasse sisse ehitatud (nn. API meetodid, mille kirjelduse leiab Java dokumentatsioonist). S�ltumata sellest, kust meetod p�rineb, v�ib see olla kas klassi- v�i isendimeetod.
//
//		Klassimeetod  (class method) , mida Javas kirjeldab v�tmes�na static, on kasutatav n.�. "igas olukorras", s.t. ei ole vajalik objektorienteeritud paradigma j�rgimine (esialgu p��ame oma kursuses l�bi ajada klassimeetoditega). T�psemalt �eldes - klassimeetodi poole p��rdumiseks ei ole vajalik objekti olemasolu.
//		Klassimeetodi poole p��rdumiseks kirjutatakse reeglina:
//		Klassi_nimi . meetodi_nimi ( faktilised_parameetrid );
//		Kui meetod on defineeritud jooksvas klassis, siis v�ib klassi nime (ja punkti) �ra j�tta.
//
//		N�it.     Math.sqrt (2.);
//
//
//
//		Isendimeetod (instance method) on rakendatav mingile etteantud objektile (seda objekti t�histatakse meetodi kirjelduses v�tmes�naga this). Isendimeetodi rakendamist nim. ka teate saatmiseks objektile.
//		Isendimeetodi poole p��rdumiseks kirjutatakse reeglina:
//		objekt . meetodi_nimi ( faktilised_parameetrid );
//		Kui isendimeetodi poole p��rdumisel on objektiks this, siis v�ib selle (ja punkti) �ra j�tta.
//
//		N�it.     "Tere hommikust!" .length();
//
//
//
//		Meetodi poole p��rdumine (method call)  toimub faktiliste e. tegelike parameetritega, s.t. meetodi nime j�rele kirjutatakse �marsulgudesse sobivat t��pi avaldised. Kui parameetrite arv on null, siis tuleb Javas ikkagi kirjutada t�hjad sulud (et eristada meetodit muutujast).
//
//		Meetodi defineerimisel kasutame formaalseid parameetreid, mis seatakse tegelike parameetritega vastavusse meetodi poole p��rdumisel. Javas seostatakse parameetrid positsiooni j�rgi, s.t. oluline on t�pne parameetrite j�rjestus. Ka v�tmes�na this v�ib k�sitleda formaalse parameetrina, millele vastab p��rdumisel punkti ees olev objekt.
//		Lisaks sellele m��ratakse meetodi defineerimisel alati nn. tagastust��p (s.t. mis t��pi v��rtus on meetodi t�� tulemuseks). Tagastusv��rtuse puudumisel on tagastust��biks void. Tagastusv��rtuse m��rab meetodis t�idetava return-lause j�rel olev avaldis (void-meetoditel on return-lause ilma avaldiseta).
//
//		Sisendparameetrid on meetodile algandmeteks, mida ei muudeta. V�ljundparameetrid (Java korral on ainsaks v�ljundparameetriks tagastusv��rtus) on meetodi t�� tulemuseks. Sisend-v�ljundparameetrid on korraga m�lemas rollis (s.t. neid muudetakse meetodi t�� k�igus), olles Javas siiski s�ntaktiliselt samav��rsed sisendparameetritega.
//
//		Kui meetod ei tegele sisendi/v�ljundiga ning ei muuda keskkonna seisu kaudselt (n�iteks muutes parameetrite kaudu k�ttesaadavaid objekte), siis nim. seda k�rvalefektideta meetodiks.
//
//
//		Meetodi    signatuuriks on meetodi nimi, parameetrite t��bid ja tagastusv��rtuse t��p.
//
//
//		N�ide:
//
//		Klassimeetodid: main, syt
//		Formaalsed parameetrid: main-meetodi korral param,    syt korral a ja b
//		Faktilised parameetrid: syt korral m ja n
//		Tagastusv��rtus: a
//
//		public class Euclid {
//
//		   public static void main (String[] param) {
//		      int m=15;
//		      int n=6;
//		      if (param.length > 1) {
//		         m=Integer.parseInt (param [0]);
//		         n=Integer.parseInt (param [1]);
//		      }
//		      System.out.println ("SYT (" + m + ", " + n
//		         + ") = " + syt (m, n));
//		   } // main
//
//		   public static int syt (int a, int b) {
//		      while (b != 0) {
//		         int j22k = a % b;
//		         a = b;
//		         b = j22k;
//		      }
//		      return a;
//		   } // syt
//
//		} // Euclid
//
//
//
//		Massiiv
//
//		Kui muutujaid on v�he, siis pole ka probleemi neile nimede leidmisega. N�iteks ruutv�rrandi lahendamise programmis leidsime kaks lahendit ja v�isime neid nimetada x1 ja x2.  Kui peaksime aga arvutama 1000 v��rtust mingi rutiinse reegli j�rgi, siis oleks v�ga ebamugav kirjeldada 1000 eraldi muutujat.  Ka tavaelus oleks raske linnas orienteeruda, kui majad poleks t�navate kaupa nummerdatud, vaid igal neist oleks oma nimi (isegi Inglismaal hakatakse sellest aru saama). Seda nummerdamise ideed kannab programmeerimises massiivi m�iste.
//
//		Massiiv on andmestruktuur, mis lubab samat��bilisi andmeid koondada �hise nime alla ning teha andmeelementidel vahet j�rjekorranumbri (indeksi) j�rgi. �ldisemal juhul v�ib indekseid olla rohkem kui �ks - nii saadakse mitmem��tmelised massiivid. Mitmem��tmelist massiivi saab k�sitleda kui massiivi, mille elementideks on omakorda massiivid (Javas ka nii tehakse).
//		�hem��tmelist (�he indeksiga) massiivi nim. ka j�rjendiks, kahem��tmelist massiivi maatriksiks v�i tabeliks.
//
//		Massiivi iseloomustavad seega:
//
//		    massiivi nimi (t�psemalt massiivi identifitseeriv L-v��rtus)
//		    massiivi elemendi t��p
//		    massiivi indeksite arv ja indeksite t��bid
//		    massiivi elementide arv (t�psemalt iga indeksi v�imalike v��rtuste hulk)
//		    massiivi elementide v��rtused
//
//		Javas k�sitletakse massiive �hem��tmelistena, kahem��tmeline massiiv on �hem��tmeliste massiivide massiiv jne.
//		Javas on massiivi indeksiks t�isarv vahemikus 0 kuni massiivi pikkus miinus �ks.
//		Massiiv on massiivit��pi muutuja (L-v��rtus). Javas saab massiivi kirjeldada ilma massiivi elementide arvu fikseerimata. Elementide arv m��ratakse m�lu reserveerimise k�igus (see operatsioon on Javas massiivi kirjeldusest lahutatud).
//		Javas kasutatakse massiivi elemendile viitamiseks indeksit, mis kirjutatakse massiivi nime j�rele kantsulgudesse. Massiivi element on n�ide L-v��rtusest, s.t. massiivi elemendile saab omistada v��rtust.
//		Massiivi elementide arvu Javas (massiivi pikkust) v�ljendab Javas avaldis "massiivi_nimi .length" (massiiv on objekt, mille avalik read-only isendimuutuja nimega length sisaldab massiivi pikkust).
//
//		N�ide.
//
//
//		      int [] m;                       // massiivi kirjeldamine
//
//		      m = new int [10];               // m�lu reserveerimine massiivile
//
//		      System.out.println (m.length);  // massiivi pikkuse v�ljastamine
//
//		      m[0] = 3;                       // omistamine elemendile indeksiga 0
//		      m[1] = -8;
//
//		      // massiivi v�ljastamine for-ts�kli abil
//		      for (int i=0; i<m.length; i++)
//		         System.out.print (String.valueOf (m[i]) + " ");
//		      System.out.println();
//		  
//
//
//		Massiivi kirjeldamist, m�lu eraldamist ja massiivi elementide v��rtustamist saab Javas teha korraga nn. massiivialgati abil.
//
//		int [] a = {3, -8, 0, 0, -1, 0, 0};
//
//
//		Luuakse 7-elemendiline t�isarvude massiiv loetletud v��rtustest ( a[0]==3, a[1]==-8, ..., a[6]==0 ).
//
//		Seda saab  kirjutada ka massiivit��pi avaldisena:
//		new int [] {3, -8, 0, 0, -1, 0, 0}
//
//		Vt. joonist
//
//
//		Javas saab mitmem��tmelise massiivi korral iga �lemisel tasemel kirjeldatud massiivi element olla erineva pikkusega massiiv.
//
//		N�ide.
//
//		        int [][] m;                       // massiivi kirjeldamine
//		      m = new int [2][];                // m�lu reserveerimine esimesel tasemel
//		      System.out.println (m.length);    // massiivi pikkus (esimene tase)
//		      m[0] = new int [4];               // m�lu rerveerim. teisel tasemel
//		      m[0][0] = -8;                     // omistamine elemendile
//		      m[1] = new int [3];
//		      m[1][0] = 9;
//
//		      // massiivi v2ljastamine for-tsyklite abil
//		      for (int i=0; i<m.length; i++) {
//		         for (int j=0; j<m[i].length; j++)
//		            System.out.print (String.valueOf (m[i][j]) + " ");
//		         System.out.println();
//		      } // for i
//
//		Vt. joonist
//		J�rjendi t��tlemine
//		Vaatleme n�ite�lesandeid t��ks �hem��tmelise massiiviga.
//
//		Maksimumi leidmine
//		   public static int maks (int[] m) {
//		      int res = Integer.MIN_VALUE;
//		      for (int i=0; i<m.length; i++) {
//		         if (m[i] > res)
//		            res = m[i];
//		      } // for i
//		      return res;
//		   } // maks
//
//		Vastupidise elementide j�rjekorraga massiivi leidmine
//		   public static int [] reverse (int [] m) {
//		      int[] res = new int [m.length];
//		      for (int i=0; i<m.length; i++) {
//		         res [res.length - 1 - i] = m [i];
//		      }
//		      return res;
//		   } // reverse
//
//
//		M�rgimuutude arvu leidmine
//
//		   public static int mmArv (int [] m) {
//		      int res = 0;
//		      if (m.length < 2) return 0;
//		      for (int i=0; i<m.length-1; i++) {
//		         if (m[i]>=0 && m[i+1]<0) res++;
//		         if (m[i]<0 && m[i+1]>=0) res++;
//		      }
//		      return res;
//		   } // mmArv
//
//
//		Muutuva parameetrite arvuga meetodi kirjeldamine Javas
//
//		    Alates Java 1.5 saab meetodi parameetreid "pakkida" massiiviks, vt. n�ide:
//
//		        import java.util.*;
//
//		        public class Proov {
//
//		           public static void main (String[] args) {
//		              mm ("tere", "kaunis", "hommik");
//		           }
//
//		           public static void mm (String ... s) {
//		              System.out.println (s.length + " " + Arrays.toString(s));
//		           }
//
//		        }

	}

}
