package snake2;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Keys extends KeyAdapter {

	@Override
	public void keyReleased(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			if (SnakeCanvas.direction != Finals.SOUTH) {
				SnakeCanvas.direction = Finals.NORTH;
			}
			break;
		case KeyEvent.VK_DOWN:
			if (SnakeCanvas.direction != Finals.NORTH) {
				SnakeCanvas.direction = Finals.SOUTH;
			}
			break;
		case KeyEvent.VK_RIGHT:
			if (SnakeCanvas.direction != Finals.WEST) {
				SnakeCanvas.direction = Finals.EAST;
			}
			break;
		case KeyEvent.VK_LEFT:
			if (SnakeCanvas.direction != Finals.EAST) {
				SnakeCanvas.direction = Finals.WEST;
			}
			break;
		}

	}

}
