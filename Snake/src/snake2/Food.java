package snake2;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Random;

public class Food {

	public Point food;

	/*
	 * Maaran toidule suvalise asukoha, mis on akna piirdes.
	 */
	public void placeFood() {
		Random rand = new Random();
		int randomX = rand.nextInt(Finals.GRID_WIDTH);
		int randomY = rand.nextInt(Finals.GRID_HEIGHT);
		Point randomPoint = new Point(randomX, randomY);
		/*
		 * Teen loopi, mis genereerib uue toidu suvalisse kohta, kui uss on
		 * selle soonud.
		 */
		while (SnakeCanvas.snakeBody.contains(randomPoint)) {
			randomX = rand.nextInt(Finals.GRID_WIDTH);
			randomY = rand.nextInt(Finals.GRID_HEIGHT);
			randomPoint = new Point(randomX, randomY);
		}
		food = randomPoint;
	}

	/*
	 * Joonistab toidu.
	 */
	public void drawFood(Graphics g) {
		g.setColor(Color.RED);
		g.fillOval(food.x * Finals.BOX_WIDTH, food.y * Finals.BOX_HEIGHT, Finals.BOX_WIDTH, Finals.BOX_HEIGHT);
		g.setColor(Color.BLACK);
	}
}