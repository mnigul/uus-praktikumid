package praktikum6;

import lib.TextIO;

public class Arvamismang {

	public static void main(String[] args) {
		int arvutiArv = suvalineArv(1, 100);
		System.out.println("Arva ära number " + arvutiArv);

		while (true) {
			int kasutajaArvas = TextIO.getlnInt();
			if (arvutiArv == kasutajaArvas) {
				System.out.println("Arvasid ära! Juhuu!");
				break;
			} else if (arvutiArv > kasutajaArvas) {
				System.out.println("See arv on suurem");
			} else {
				System.out.println("See arv on väiksem");
			}

		}

	}

	public static int suvalineArv(int min, int max) {
		int arv = (int) (Math.random() * (100 - 1)) + 1;
		return arv;
		// public static int suvalineArv(int min, int max){
		// int vahemik = max-min+1
		// return(int) (Math.random() *vahemik)+min;
	}

}
//http://devsolvd.com/questions/java-generate-random-number-between-two-given-values-duplicate