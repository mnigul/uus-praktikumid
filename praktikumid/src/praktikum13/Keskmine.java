package praktikum13;

import java.util.ArrayList;
import java.util.List;

public class Keskmine {

	public static void main(String[] args) {
		
		//kutsun valja olemasoleva meetodi
		String kataloogitee = Keskmine.class.getResource(".").getPath();
		ArrayList<String> failisisu = tekstiFailid.loeFail(kataloogitee + "numbrid.txt");
		System.out.println(failisisu);
		
		//converdin stringi doubleks
		List<Double> newList = new ArrayList<Double>(failisisu.size()) ;
        for (String arv : failisisu) { 
            newList.add(Double.valueOf(arv)); 
          }
          System.out.println(newList);
		
		//double nr = Double.parseDouble(failisisu);
	}
	
	public static double average(List<Integer> newList) {
		// 'average' is undefined if there are no elements in the list.
	    if (newList == null || newList.isEmpty())
	        return 0.0;
	    // Calculate the summation of the elements in the list
	    long sum = 0;
	    int n = newList.size();
	    // Iterating manually is faster than using an enhanced for loop.
	    for (int i = 0; i < n; i++)
	        sum += newList.get(i);
	    // We don't want to perform an integer division, so the cast is mandatory.
	    return ((double) sum) / n;
	    //http://stackoverflow.com/questions/17694532/how-to-select-all-the-occurrences-of-a-word-in-eclipse
	}

}
