package praktikum13;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;

public class tekstiFailid {
	
	public static ArrayList<String> loeFail(String failinimi){
		
		
		
		ArrayList<String> read = new ArrayList<String>();
		
		// otsime samast kataloogist kala.txt-nimelist faili
		File file = new File(failinimi);
		
		try {
			// avame faili lugemise jaoks
			BufferedReader in = new BufferedReader(new FileReader(file));
			String rida;
			
			// loeme failist rida haaval
			while ((rida = in.readLine()) != null) {
				read.add(rida);
			}
		}
		catch (FileNotFoundException e) {
			System.out.println("Faili ei leitud: \n" + e.getMessage());
		}
		catch (Exception e) {
			System.out.println("Error, jee, mingi muu error: " + e.getMessage());
		}
		
		return read;
		
	}
	
	
	public static void main(String[] args) {
		
		// punkt tähistab jooksvat kataloogi
		String kataloogitee = tekstiFailid.class.getResource(".").getPath();
		ArrayList<String>failisisu = loeFail(kataloogitee + "kala.txt");
//		System.out.println(failisisu); trykib valja ilma abc jrk
		Collections.sort(failisisu);
		System.out.println(failisisu);
		
	}
	}
//http://stackoverflow.com/questions/5343689/java-reading-a-file-into-an-arraylist
//http://www.avajava.com/tutorials/lessons/how-do-i-alphabetically-sort-the-lines-of-a-file.html%3Bjsess..