package praktikum3;

import lib.TextIO;

public class Tehisintellekt {

	public static void main(String[] args) {

		System.out.println("Sisesta esimene vanus.");
		int esimeneVanus = TextIO.getlnInt();
		System.out.println("Sisesta teine vanus.");
		int teineVanus = TextIO.getlnInt();

		if ((esimeneVanus - teineVanus > 5 && esimeneVanus - teineVanus <= 10) || (esimeneVanus - teineVanus < -5 && esimeneVanus - teineVanus >= -10)) {
			System.out.println("Midagi krõbetat");
		} else if (esimeneVanus - teineVanus > 10 || esimeneVanus - teineVanus < -10) {
			System.out.println("Midagi veel krõbedamat");
		} else {
			System.out.println("Sobib");
		}

	}
}
