package praktikum3;

import lib.TextIO;

public class CumLaude {

	public static void main(String[] args) {
		System.out.println("Sisesta oma keskmine hinne.");
		double keskmineHinne = TextIO.getlnDouble ();
		System.out.println("Sisesta oma lõputöö hinne.");
		double lõputööHinne = TextIO.getlnDouble ();
		
		
		if ((keskmineHinne >= 4.5 && keskmineHinne <= 5) && lõputööHinne == 5) {
			//see plokk täidetakse juhul, kui tingimus vastab tõele
			System.out.println("Sa saad cum laude diplomile, palju õnne!");
		} else {
			System.out.println("No diploma for you!");
		}
		
		
		
	}

}
