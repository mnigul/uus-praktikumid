package praktikum5;

import lib.TextIO;

public class Meetodid2 {

	public static void main(String[] args) {
		System.out.println(kasutajaSisestus(1, 10));

	}

	public static int kasutajaSisestus(int min, int max){
		
		int kasutajaSisestus; //deklareerin väljaspool tsüklit
		
		do{
			System.out.println("Palun sisesta arv vahemikus " + min + " kuni " + max + ".");
			
			 kasutajaSisestus = TextIO.getlnInt();
		} while (kasutajaSisestus >= min && kasutajaSisestus <=max);
		
	
		return kasutajaSisestus;
	}
	public static int kasutajaSisestus (String string, int i, int j){
		System.out.println(string);
		return kasutajaSisestus(i, j);
	}
}