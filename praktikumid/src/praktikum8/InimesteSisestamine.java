package praktikum8;

import java.util.ArrayList;

import lib.TextIO;

public class InimesteSisestamine {
	
	public static void main(String[] args) {
		
		ArrayList<Inimene> inimesed = new ArrayList<Inimene>();
		
		while (true){
			
			System.out.println("Sisesta inimene ja vanus");
			String nimi = TextIO.getlnString();
			if(nimi.equals(""))
				break;
			int vanus = TextIO.getlnInt();
			Inimene keegi = new Inimene(nimi, vanus);
			inimesed.add(keegi);
			
		}
		
		
		for (Inimene inimene : inimesed){
			//java kutsub v2lja Inimese klassi toString() meetodi
			//System.out.println(inimene);
			inimene.tervita();
		}
				
		
		
	}

}
