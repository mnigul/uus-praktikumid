package praktikum14;

public class Ring {

	Punkt keskpunkt;
	double raadius;
	
	public Ring(Punkt punkt, double r) {
		keskpunkt = punkt;
		raadius = r;
		
	}

	public double ymberm66t(){
		return 2 * Math.PI * raadius;
	}
	
	public double pindala(){
		return Math.PI * raadius * raadius;
	}
	
	@Override
	public String toString(){
		return "ring(" + keskpunkt + ", " + raadius + "( ja ymberm66t on: " 
	+ ymberm66t() + " ning pindala on: " + pindala();
	}
	
	
	
}
