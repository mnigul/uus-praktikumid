package praktikum14;

public class Katsetused {

	public static void main(String[] args) {

		Punkt minuPunkt = new Punkt();
		minuPunkt.x = 40;
		minuPunkt.y = 100;

		Punkt veelYksPunkt = new Punkt(100, 200);

		System.out.println(veelYksPunkt);
		System.out.println(minuPunkt);
		
		Joon minuJoon = new Joon(minuPunkt, veelYksPunkt);
		System.out.println(minuJoon);
		System.out.println("Joone pikkus on " + minuJoon.pikkus(minuPunkt, veelYksPunkt));
		//System.out.println(minuJoon.pikkus(minuPunkt, veelYksPunkt));
		
		Ring minuRing = new Ring (new Punkt(200,200), 23.);
		System.out.println(minuRing);
	}

}
