package praktikum14;

public class Punkt {

	int x;
	int y;

	public Punkt (int x, int y){
		
		this.x = x;//muutuja nimed samad
		this.y = y;
	}
	
	public Punkt(){
		//luuakse Punkt objekt koordinaate määramata
		
	}
	
	@Override
	public String toString(){
		
		return "Punkt(" + x + ", " + y + ")";
		
	}
	
}
