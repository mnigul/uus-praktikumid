package praktikum14;

public class Joon {
	
	Punkt algPunkt;
	Punkt l6ppPunkt;
	
	public Joon(Punkt x, Punkt y) {//muutuja nimed pole samad
		algPunkt = x;
		l6ppPunkt = y;
		
	}
	
	@Override
	public String toString(){
		return "Joon(" + algPunkt + " kuni " + l6ppPunkt + ")";
		
	}
	
	public double pikkus (Punkt algPunkt, Punkt l6ppPunkt){ //Pythagorase teoreem
		
		double a = l6ppPunkt.y - algPunkt.y; 
		double b = l6ppPunkt.x - algPunkt.x;
		
		return Math.sqrt(Math.pow(a,2) + Math.pow(b, 2));
	}

}
