package harjutamine;

public class Number3 {

	public static void main(String[] args) {
		
		int [] m = {5,10,15,-20,-10};
		
		int sum = absvSumma(m);
		
		System.out.println(sum);
		
	}

		
	public static int absvSumma (int[] m){
	
		 int sum = 0;									
		 
		 for (int i = 0; i < m.length; i++){
			 
			 if (m[i]< 0){ 
				 
				 m[i] = m[i] * (-1);
				
			 }
		
			 sum = sum + m[i];
		 }
		 
		 return sum;
	}
}