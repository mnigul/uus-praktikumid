package harjutamine;

public class V2iksemKeskmisest {

	public static void main(String[] args) {

		double m[] = { 5.0, 6.0, 5.0, 7.0 };

		System.out.println(keskmisestV2iksemaid(m));

	}

	public static int keskmisestV2iksemaid(double [] m) {
		
		double summa = 0;
		
		for (int i = 0; i < m.length; i++){
			
			summa = summa + m [i];
			
		}
		
		double keskmine = 0;
		
		keskmine = summa / m.length;
		
		int vaiksem = 0;
		
		for (int i = 0; i < m.length; i++){
			
			if (m[i] < keskmine){
				
				vaiksem = 1 + vaiksem;			
				
				
			}
		}
		
		
		
		return vaiksem;
	}
}

// Koostage Java meetod, mis leiab etteantud reaalarvude massiivi d
// põhjal niisuguste elementide arvu, mis on rangelt suuremad kõigi
// elementide aritmeetilisest keskmisest
// (aritmeetiline keskmine = summa / elementide_arv).

// public static int keskmisestParemaid (double[] d){