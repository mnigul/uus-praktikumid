package harjutamine;

public class V2iksem2 {

	public static void main(String[] args) {

		double d[] = { 5.0, 6.0, 5.0, 7.0 };

		System.out.println(allaKeskmise(d));

	}

	public static int allaKeskmise(double[] d) {
		double summa = 0;

		for (int i = 0; i < d.length; i++) {

			summa = summa + d[i];

		}

		double keskmine = 0;

		keskmine = summa / d.length;

		int vaiksem = 0;

		for (int i = 0; i < d.length; i++) {

			if (d[i] < keskmine) {

				vaiksem = 1 + vaiksem;

			}
		}

		return vaiksem;
	}
}